package mplu.sky_mobile_exercise;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import mplu.sky_mobile_exercise.api.MovieService;
import mplu.sky_mobile_exercise.presenter.MoviePresenter;
import mplu.sky_mobile_exercise.view.MovieListView;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class MoviePresenterTest {

    private MoviePresenter presenter;

    @Mock
    private MovieService service;

    @Mock
    private MovieListView view;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);// required for the "@Mock" annotations

        // Make presenter a mock while using mock repository and viewContract created above
        view = new MovieListView();
        presenter = Mockito.spy(new MoviePresenter(view));
    }

    @Test
    public void moviesRequest() {
        // Trigger
        presenter.getMoviesFromAPI();
        assertNotEquals(null,  presenter.getmMovies() );
    }

    @Test
    public void moviesSize() {
        // Trigger
        presenter.getMoviesFromAPI();
        assertEquals(12,  presenter.getmMovies().size() );
    }


}
