package mplu.sky_mobile_exercise.presenter;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import java.util.ArrayList;

import mplu.sky_mobile_exercise.R;
import mplu.sky_mobile_exercise.api.MovieService;
import mplu.sky_mobile_exercise.model.Movie;
import mplu.sky_mobile_exercise.view.MovieListView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MoviePresenter {

    private MovieListView mMovieListView;

    private MovieService mMovieService;

    private ArrayList<Movie> mMovies;

    public MoviePresenter(MovieListView view) {
        this.mMovieListView = view;
        this.mMovies = new ArrayList<>();

        if (this.mMovieService == null) {
            this.mMovieService = new MovieService();
        }
    }

    public void getMoviesFromAPI() {

        mMovieListView.showProgressBar();

        if (verifyConnection()) {

            mMovieService.getAPI().getMovies().enqueue(new Callback<ArrayList<Movie>>() {
                @Override
                public void onResponse(Call<ArrayList<Movie>> call, Response<ArrayList<Movie>> response) {
                    ArrayList<Movie> moviesResult = response.body();

                    if (moviesResult != null) {
                        setmMovies(moviesResult);
                        mMovieListView.hideProgressBar();
                        mMovieListView.updateMovies();
                    }

                }

                @Override
                public void onFailure(Call<ArrayList<Movie>> call, Throwable t) {
                    mMovieListView.hideProgressBar();
                    try {
                        throw new InterruptedException(String.valueOf(R.string.movie_api_error));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
        }else{
            mMovieListView.hideProgressBar();
            mMovieListView.showNetworkError();
        }
    }

    public boolean verifyConnection(){
        ConnectivityManager cm =
                (ConnectivityManager)mMovieListView.getApplicationContext().
                        getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();


        return isConnected;

    }

    public MovieListView getmMovieListView() {
        return mMovieListView;
    }

    public void setmMovieListView(MovieListView mMovieListView) {
        this.mMovieListView = mMovieListView;
    }

    public MovieService getmMovieService() {
        return mMovieService;
    }

    public void setmMovieService(MovieService mMovieService) {
        this.mMovieService = mMovieService;
    }

    public ArrayList<Movie> getmMovies() {
        return mMovies;
    }

    public void setmMovies(ArrayList<Movie> mMovies) {
        this.mMovies = mMovies;
    }
}
