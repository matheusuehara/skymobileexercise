package mplu.sky_mobile_exercise.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import mplu.sky_mobile_exercise.R;
import mplu.sky_mobile_exercise.model.Movie;
import mplu.sky_mobile_exercise.presenter.MoviePresenter;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.ViewHolder> {

    private MoviePresenter mPresenter;

    public MovieAdapter(MoviePresenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.movie_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        Movie movie = mPresenter.getmMovies().get(position);

        Picasso.get()
                .load(movie.getCoverUrl())
                .error(R.drawable.ic_launcher_background)
                .into(holder.mMovieImage);

        holder.mMovieName.setText(movie.getTitle());
    }

    @Override
    public int getItemCount() {
        return mPresenter.getmMovies().size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        final ImageView mMovieImage;
        final TextView mMovieName;

        ViewHolder(View view) {
            super(view);
            mMovieImage = (ImageView) view.findViewById(R.id.movie_image);
            mMovieName = (TextView) view.findViewById(R.id.movie_title);
        }
    }
}
