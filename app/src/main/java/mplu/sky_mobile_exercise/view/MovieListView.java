package mplu.sky_mobile_exercise.view;

import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import mplu.sky_mobile_exercise.R;
import mplu.sky_mobile_exercise.presenter.MoviePresenter;
import mplu.sky_mobile_exercise.view.adapter.MovieAdapter;
import mplu.sky_mobile_exercise.view.decoration.GridDecoration;

public class MovieListView extends AppCompatActivity implements MovieView {

    private final static int COLUMN_NUMBER = 2;

    private ProgressBar mProgressBar;
    private RecyclerView mRecyclerView;
    private MoviePresenter mMoviePresenter;
    private CoordinatorLayout mCoordinatorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movies_list_view);

        mCoordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinator_layout);

        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);

        mMoviePresenter = new MoviePresenter(this);

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new GridLayoutManager(this, COLUMN_NUMBER));

        mRecyclerView.setAdapter(new MovieAdapter(mMoviePresenter));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addItemDecoration(new GridDecoration(this));

        mMoviePresenter.getMoviesFromAPI();
    }

    @Override
    public void updateMovies() {
        mRecyclerView.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void showProgressBar(){
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar(){
        mProgressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showNetworkError(){
        Snackbar snackbar = Snackbar
                .make(mCoordinatorLayout,R.string.connection_error, Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.try_again, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                       mMoviePresenter.getMoviesFromAPI();
                    }
                });
        snackbar.show();
    }

}
