package mplu.sky_mobile_exercise.view;

public interface MovieView {

    void updateMovies();
    void showProgressBar();
    void hideProgressBar();
    void showNetworkError();

}