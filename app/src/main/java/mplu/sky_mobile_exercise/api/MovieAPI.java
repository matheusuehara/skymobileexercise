package mplu.sky_mobile_exercise.api;

import java.util.ArrayList;

import mplu.sky_mobile_exercise.model.Movie;
import retrofit2.Call;
import retrofit2.http.GET;

public interface MovieAPI {

    @GET("Movies")
    Call<ArrayList<Movie>> getMovies();
}