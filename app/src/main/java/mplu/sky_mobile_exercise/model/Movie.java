package mplu.sky_mobile_exercise.model;
import android.support.v7.widget.RecyclerView;

import com.google.gson.annotations.SerializedName;

public class Movie {

    @SerializedName("id")
    private String id;

    @SerializedName("title")
    private String title;

    @SerializedName("overview")
    private String overview;

    @SerializedName("duration")
    private String duration;

    @SerializedName("release_year")
    private Integer releaseYear;

    @SerializedName("cover_url")
    private String coverUrl;

    @SerializedName("backdrops_url")
    private String[] backdropsUrl;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public Integer getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(Integer releaseYear) {
        this.releaseYear = releaseYear;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
    }

    public String[] getBackdropsUrl() {
        return backdropsUrl;
    }

    public void setBackdropsUrl(String[] backdropsUrl) {
        this.backdropsUrl = backdropsUrl;
    }
}
